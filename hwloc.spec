Name:           hwloc
Version:        2.11.2
Release:        1
Summary:        Hardware locality utilities and libraries
License:        BSD-3-Clause
URL:            https://www.open-mpi.org/projects/hwloc/
Source0:        https://download.open-mpi.org/release/%{name}/v2.11/%{name}-%{version}.tar.bz2

BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(numa)
BuildRequires:  pkgconfig(pciaccess)
BuildRequires:  libX11-devel
BuildRequires:  ncurses-devel
%ifarch %{ix86} x86_64
%{?systemd_requires}
%endif
BuildRequires:  systemd

Provides:       hwloc-libs hwloc-gui hwloc-plugins
Obsoletes:      hwloc-libs hwloc-gui hwloc-plugins

%description
hwloc provides command line tools and a C API to obtain the hierarchical map of key computing elements,
such as: NUMA memory nodes, shared caches, processor sockets, processor cores, and processor "threads".
hwloc also gathers various attributes such as cache and memory information,
and is portable across a variety of different operating systems and platforms.

%package        devel
Summary:        Development files for hwloc
Requires:       %{name} = %{version}-%{release}

%description    devel
Contains header files, development libraries and shared object symbolic links for hwloc.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
export runstatedir=/run
%configure --enable-plugins --disable-silent-rules
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make_build

%install
%make_install
%delete_la

cp -pr doc/examples %{buildroot}%{_pkgdocdir}
# Fix for BZ1253977
mv  %{buildroot}%{_pkgdocdir}/examples/Makefile  %{buildroot}%{_pkgdocdir}/examples/Makefile_%{_arch}

# Make hwloc-gui not depend on hwl
rm %{buildroot}%{_mandir}/man1/lstopo.1
ln %{buildroot}%{_mandir}/man1/lstopo-no-graphics.1 %{buildroot}%{_mandir}/man1/lstopo.1

# https://github.com/open-mpi/hwloc/issues/221
# This binary is built only for intel architectures
%ifarch %{ix86} x86_64
install -d %{buildroot}%{_unitdir}
mv %{buildroot}%{_datadir}/%{name}/hwloc-dump-hwdata.service %{buildroot}%{_unitdir}/
%else
rm %{buildroot}%{_datadir}/%{name}/hwloc-dump-hwdata.service
%endif

%check
LD_LIBRARY_PATH=$PWD/hwloc/.libs %make_build check

%ifarch %{ix86} x86_64
%post
%systemd_post hwloc-dump-hwdata.service
%preun
%systemd_preun hwloc-dump-hwdata.service
%postun
%systemd_postun_with_restart hwloc-dump-hwdata.service
%endif

%files
%license COPYING
%doc AUTHORS NEWS README
%{_bindir}/%{name}*
%{_bindir}/lstopo*
%{_datadir}/%{name}
%{_datadir}/applications/lstopo.desktop
%{_datadir}/bash-completion/completions/hwloc
%ifarch %{ix86} x86_64
%{_sbindir}/hwloc-dump-hwdata
%{_unitdir}/hwloc-dump-hwdata.service
%endif
%{_libdir}/%{name}
%{_libdir}/libhwloc*so.15*

%files devel
%{_libdir}/pkgconfig/*.pc
%{_includedir}/%{name}
%{_includedir}/%{name}.h
%{_libdir}/*.so
%{_pkgdocdir}/examples

%files help
%{_pkgdocdir}/*.html
%{_pkgdocdir}/*.pdf
%{_mandir}/man?/*

%changelog
* Fri Sep 27 2024 Funda Wang <fundawang@yeah.net> - 2.11.2-1
- update to 2.11.2

* Thu Aug 01 2024 Funda Wang <fundawang@yeah.net> - 2.11.1-1
- update to 2.11.1

* Thu Mar 21 2024 xuhe <xuhe@kylinos.cn> - 2.10.0-2
- Fix Source0 info

* Mon Mar 18 2024 xu_ping <707078654@qq.com> - 2.10.0-1
- Upgrade version to 2.10.0

* Wed Aug 30 2023 wangkai <13474090681@163.com> - 2.9.1-2
- Fix CVE-2022-47022

* Thu Apr 13 2023 liyanan <thistleslyn@163.com> - 2.9.1-1
- Update to 2.9.1

* Sun May 22 2022 Yicong Yang <young.yicong@outlook.com> - 2.7.1-1
- Update to 2.7.1 stable

* Mon May 9 2022 caodongxia <caodongxia@h-partners.com> - 1.11.9-5
- License compliance rectification

* Thu Jul 22 2021 shixuantong <shixuantong@huawei.com> - 1.11.9-4
- move %{_pkgdocdir} to help subpackage

* Fri Nov 15 2019 chenzhenyu <chenzhenyu13@huawei.com> - 1.11.9-3
- Package init
